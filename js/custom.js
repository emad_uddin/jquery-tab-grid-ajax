(function ($) {
    "use strict";

    $(document).ready(function () {

    
    $('#tabs').on("click", "li a", function () {
        var tabsId = $(this).attr("id");
        $(this).addClass("active").siblings().removeClass("active");
        $('#' + tabsId + 'C').addClass("active").siblings().removeClass("active");
        $('#tabs li a').addClass('disable');
        $(this).removeClass('disable');
        
    });
    
    $("#tabs").on("click", "li a", function(){
        var tabsId = $(this).attr("id");
        $.ajax({url: "tab1.html", success: function(result){
            $('#' + tabsId + 'C').html(result);
        }});
    });
    
    $("#tabs").on("click", "li a", function(){
        var tabsId = $(this).attr("id");
        $.ajax({url: "tab2.html", success: function(result){
            $('#' + tabsId + 'C').html(result);
        }});
    });
    
    $("#tabs").on("click", "li a", function(){
        var tabsId = $(this).attr("id");
        $.ajax({url: "tab3.html", success: function(result){
            $('#' + tabsId + 'C').html(result);
        }});
    });
        
    $("#tabs").on("click", "li a", function(){
        var tabsId = $(this).attr("id");
        $.ajax({url: "tab4.html", success: function(result){
            $('#' + tabsId + 'C').html(result);
        }});
    });
    
});


})(jQuery);
